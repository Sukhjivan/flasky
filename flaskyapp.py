from flask import Flask, jsonify, request
import os

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))


@app.route('/')  # The '/' indicated the home of a website, i.e. www.reddit.com/
def home():
    return jsonify(data="My Simple Homepage - Welcome!!!")


if __name__ == '__main__':
    app.run()